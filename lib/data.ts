import React from "react";
import { CgWorkAlt } from "react-icons/cg";
import { FaReact } from "react-icons/fa";
import { LuGraduationCap } from "react-icons/lu";
import legomoduleImg from "@/public/lego-module.png";
import designerPlatform from "@/public/designerPlatform.png";
import dzqImg from "@/public/dzq.jpeg";
import lowcodeImg from "@/public/lowcode.png";

export const links = [
  {
    name: "Home",
    hash: "#home",
  },
  {
    name: "About",
    hash: "#about",
  },
  {
    name: "Projects",
    hash: "#projects",
  },
  {
    name: "Skills",
    hash: "#skills",
  },
  {
    name: "Experience",
    hash: "#experience",
  },
  {
    name: "Contact",
    hash: "#contact",
  },
] as const;

export const experiencesData = [
  {
    title: "Front-End Developer",
    location: "China, Guangzhou",
    description:
      "Mainly responsible for Maiyuan Innovation Company's internal project product module customizer, designer platform, consumer synthesis services, various website landing page development, etc., and research on the implementation of various preview effects. Launch various new e-commerce products with customized functions according to the operational needs of the mall site, and quickly integrate functional modules into the corresponding site. Provide rich customizable functional products for the company's thousands of shopify sites, greatly improving product conversion rates.",
    icon: React.createElement(FaReact),
    date: "2021 - present",
  },
  {
    title: "Front-End Developer",
    location: "China, Guangzhou",
    description:
      "Responsible for the theme development of the Youhaosuda platform, providing Sass services such as corporate portals, brand malls, and mini programs, and independently completing various Friendly Sudden mall management platforms, web-side applications, and mini programs. After the company became one of Tencent Cloud website building service providers, it participated in the zero-to-one development of the Vue version of the DZQ forum open source project, and subsequently provided h5 associated with the friendly quick-build platform sass system for the Tencent Cloud micro-building low-code platform. with applet template.",
    icon: React.createElement(CgWorkAlt),
    date: "2020 - 2021",
  },
  {
    title: "Front-End Developer(intern)",
    location: "China, Guangzhou",
    description:
      "Mainly responsible for developing the front-end interface and functions of the official website of Roaming Technology Company. Responsible for ensuring the compatibility and responsive layout of the website on various devices, docking the back-end data interface, ensuring normal data interaction between the front-end and the back-end, and quickly following up on bugs and fixing them in a timely manner according to testing requirements.",
    icon: React.createElement(LuGraduationCap),
    date: "2020",
  },
] as const;
export const experiencesDataZh = [
  {
    title: "前端开发",
    location: "广州",
    description:
      "主要负责迈远创新公司内部项目商品模块定制器,设计器平台,消费者合成服务,各种站点落地页开发等,调研各种预览效果的实现。根据商城站点运营需求上新各种拥有定制化功能的电商产品,并快速集成功能模块到对应站点。为公司上千个 shopify站点提供丰富的可定制化功能产品,大大提升商品转化率。",
    icon: React.createElement(CgWorkAlt),
    date: "2021 - present",
  },
  {
    title: "前端开发",
    location: "广州",
    description:
      "负责友好速搭平台主题开发,提供企业门户,品牌商城,小程序等sass服务,独立完成各种友好速搭商城管理平台,web端应用和小程序。公司成为了腾讯云建站服务商之一后,参与了vue版本的DZQ论坛开源项目从零到一开发的工作中,后续为腾讯云微搭低代码平台提供与友好速搭平台sass系统关联的h5与小程序模板。",
    icon: React.createElement(CgWorkAlt),
    date: "2020 - 2021",
  },
  {
    title: "前端开发(实习)",
    location: "广州",
    description:
      "主要负责开发漫游科技公司官网的前端界面和功能。负责确保网站在各种设备上的兼容性和响应式布局,对接后端数据 接口,确保前端与后端的数据交互正常,根据测试要求,快速跟进bug并及时修复。",
    icon: React.createElement(CgWorkAlt),
    date: "2019-2020",
  },
] as const;

export const projectsData = [
  {
    title: "Visual low-code platform",
    description:
      "The project mainly meets the needs of designers to design product promotion posters, product main images, marketing activities and other poster generation and production scenarios.",
    content: `<p style="margin: 10px 0px">1. Participate in the design and discussion of business component libraries, visual editors, SSR rendering H5 page services, self-developed statistical services, business management backend, etc., and investigate the feasibility of the solutions.</p>
    <p style="margin: 10px 0px">2. Responsible for attribute design in the business component library, developing, testing, rollup packaging, publishing npm based on component scalability and reusability, and adding CI/CD processes.</p>
   <p style="margin: 10px 0px">3. Responsible for the implementation of visual editor real-time form, component movement, scaling, right-click menu operations and global shortcut keys, canvas redo/undo, work preview and multi-channel publishing.</p>
    <p style="margin: 10px 0px">4. Responsible for researching and designing and developing Vue3 SSR rendering solutions, processing business component server-side rendering, processing H5 pages to be 100% responsive and adapting to different device sizes, speeding up web page access through static resource js and css uploading to cdn, and assigning H5 page events and channels Account checking, channel statistics, and WeChat sharing functions.</p>
    <p style="margin: 10px 0px">5. Responsible for the design and development of self-researched statistical services, supporting custom event statistics, log collection, log analysis, h5 page multi-channel access statistics, and providing open api to the management backend for display through Echart to achieve closed-loop business.</p>`,
    tags: [
      "Vue3",
      "Vue-jest",
      "Antd",
      "Koa",
      "Mongodb",
      "Typescript",
      "Mysql",
      "Redis",
    ],
    imageUrl: lowcodeImg,
    link: "",
    link2: "",
  },
  {
    title: "Product Customizer Module Libaries",
    description:
      "the project independently develops customizable functional modules for the product, provides customers with preview",
    content: `<p style="margin: 10px 0px">1. Use Dumi and Lerna for monorepo multi-package management, standardize code formats and submission specifications such as husky and CommitLint, promote the project to open msfu, improve the project hot update speed, and shorten the project startup to less than a few seconds.</p>
    <p style="margin: 10px 0px">2. Conduct research based on needs and develop customized functional modules. Use Fabric.js to draw complex effects of products while ensuring compatibility with PC and mobile terminals.</p>
   <p style="margin: 10px 0px"> 3. Extract common components, tool classes, and hooks, and use Qiniu Cloud Oss to organize resource maps, compress, crop images, automatically transfer packages, and perform regular Webpack optimization.</p>
    <p style="margin: 10px 0px">4. Integrate the module into the corresponding site such as Shopify, connect the product composite image with the back end, and adjust the front end to generate the composite image according to the test order.</p>
    <p style="margin: 10px 0px">5. Use Gtag to record statistical points such as product click-through rate and car-adding rate, and make data statistics to provide support for product function expansion.</p>`,
    tags: [
      "React",
      "Canvas",
      "Fabric.js",
      "Dumi",
      "Three.js",
      "Typescript",
      "D3",
      "I18n",
    ],
    imageUrl: legomoduleImg,
    link: "https://my-modules.netlify.app/modules/lego-mini",
    link2: "https://minefigs.com/products/custom-lego-minifigures",
  },
  {
    title: "Designer Platform",
    description:
      "The platform includes main image center, customized design, material maintenance, abnormal orders, store management and other sections, providing the company's product managers and designers with help in product self-design, planning and customization.",
    tags: ["React", "Canvas", "Fabric.js", "Umi", "Three.js", "Redux"],
    imageUrl: designerPlatform,
    content: `<p style="margin: 10px 0px">1. Design and develop custom functional components on the c-side and publish them as an npm package. Optimize the performance and customizability of these components to make them easier to integrate and extend.</p>
    <p style="margin: 10px 0px">2. Divide user permissions according to the RABC permission system, launch a universal login scheme, user exit scheme, dynamic sidebar scheme, etc. to solve common problems on the platform and make it easier for users to operate the platform.</p>
    <p style="margin: 10px 0px"> 3. Develop a visual drag-and-drop platform for B-end customization functions, and provide configurations such as product variants, templates, and direct delivery to the factory. Analyze the number of new products on the shelves on a monthly basis, and analyze that the platform can effectively increase the product newness rate by an average of 80%.</p>
    <p style="margin: 10px 0px">4. Use Echarts to develop various visual data platforms to conduct data analysis and reporting on product customization, abnormal orders, and factory test order feedback, solving the difficulties of large amounts of data analysis, providing effective analysis of operations and reducing workload by 40%.</p>
    <p style="margin: 10px 0px">5. Use gitlab webhook to connect to Feishu Robot to provide project updates and release-related information to specific groups, helping products and tests to quickly follow up on product development progress.</p>`,
    link: "",
    link2: "",
  },
  {
    title: "DiscuzQ",
    description:
      "This project is an open source project of Tencent Cloud. It aims to reconstruct community products, and create new forum products with the purpose of being lightweight and easy to monetize. It also provides DZQ Forum e-commerce version lays the foundation.",
    tags: ["Vue", "Nuxt.js", "Vue", "Vuex", "Element UI", "Vue-Router"],
    imageUrl: dzqImg,
    content: `<p style="margin: 10px 0px">1. Responsible for developing sections such as username, phone number, WeChat login/register via QR code, personal center, and personal profile. Used Vuex for centralized management of user information, login status, and personal settings, enabling shared state between components.</p> 
    <p style="margin: 10px 0px">2. Implemented Axios request interceptors for unified request handling, including adding authentication information and error handling. Implemented route authorization using whitelist configuration to ensure that only authorized users can access specific pages or features.</p> 
    <p style="margin: 10px 0px">3. Utilized Shell scripts for quick deployment and release, simplifying the deployment process and improving development and release efficiency.</p> 
    <p style="margin: 10px 0px">4. Implemented long list optimization on the homepage using vue-virtual-scroller, enhancing rendering performance and user experience. Additionally, leveraged Webpack for JavaScript and CSS compression, code chunking, and lazy loading of components to further optimize page loading speed and performance.</p>`,
    link: "https://my-modules.netlify.app/modules/lego-mini",
    link2: "https://minefigs.com/products/custom-lego-minifigures",
  },
] as const;

export const projectsDataZh = [
  {
    title: "可视化低代码平台",
    description:
      "项目主要满足运营、设计师等设计商品宣传海报、商品主图,营销活动等海报生成和制作场景。",
    content: `<p style="margin: 10px 0px">1. 参与业务组件库,可视化编辑器,SSR渲染H5页服务,自研统计服务,业务管理后台等架构设计与讨论,调研方案可行性。</p>
    <p style="margin: 10px 0px">2. 负责业务组件库中属性设计,根据组件拓展性,可复用性进行开发、测试、rollup打包、发布npm,添加CI/CD流程。</p>
   <p style="margin: 10px 0px">3. 负责可视化编辑器实时表单,组件移动,缩放,右键菜单操作与全局快捷键,画布重做/撤销,作品预览和多渠道发布实现。</p>
    <p style="margin: 10px 0px">4. 负责调研Vue3 SSR渲染方案并设计开发,处理业务组件服务端渲染,处理H5页尽量100%响应式适配不同设备尺寸,通过静态资源js,css上传cdn加快网页访问速度,赋予H5页事件,渠道号检查,渠道统计,微信分享功能。</p>
    <p style="margin: 10px 0px">5. 负责自研统计服务设计开发,支持自定义事件统计,进行日志收集,日志分析,h5页多渠道访问统计,提供open api给予管理后台通过Echart展示,实现业务闭环。</p>`,
    tags: [
      "Vue3",
      "Vue-jest",
      "Antd",
      "Koa",
      "Mongodb",
      "Typescript",
      "Mysql",
      "Redis",
    ],
    imageUrl: lowcodeImg,
    link: "",
    link2: "",
  },
  // {
  //   title: "社区平台threads",
  //   description:
  //     "全栈式社区平台,支持贴文发布,搜索其他用户,组织团队划分,贴文回复提醒",
  //   content: `<p style="margin: 10px 0px">1. 参与业务组件库,可视化编辑器,SSR渲染H5页服务,自研统计服务,业务管理后台等架构设计与讨论,调研方案可行性。</p>
  //   <p style="margin: 10px 0px">2. 负责业务组件库中属性设计,根据组件拓展性,可复用性进行开发、测试、rollup打包、发布npm,添加CI/CD流程。</p>
  //  <p style="margin: 10px 0px">3. 负责可视化编辑器实时表单,组件移动,缩放,右键菜单操作与全局快捷键,画布重做/撤销,作品预览和多渠道发布实现。</p>
  //   <p style="margin: 10px 0px">4. 负责调研Vue3 SSR渲染方案并设计开发,处理业务组件服务端渲染,处理H5页尽量100%响应式适配不同设备尺寸,通过静态资源js,css上传cdn加快网页访问速度,赋予H5页事件,渠道号检查,渠道统计,微信分享功能。</p>
  //   <p style="margin: 10px 0px">5. 负责自研统计服务设计开发,支持自定义事件统计,进行日志收集,日志分析,h5页多渠道访问统计,提供open api给予管理后台通过Echart展示,实现业务闭环。</p>`,
  //   tags: [
  //     "React",
  //     "Nextjs",
  //     "Clerk",
  //     "TailWind Css",
  //     "Zod",
  //     "Typescript",
  //     "Mongodb",
  //     "Shadcn UI",
  //   ],
  //   imageUrl: lowcodeImg,
  //   link: "",
  //   link2: "",
  // },
  {
    title: "商品定制器模块",
    description:
      "项目根据商品特性和优势,为商品独立开发可定制功能模块,为客户提供最真实的商品预览效果,独一无二的自我定制功能。",
    content: `<p style="margin: 10px 0px">1. 使用Dumi及Lerna进行monorepo多包管理,husky和CommitLint等规范代码格式与提交规范,推进项目开启msfu提 高项目热更新速度,项目启动缩到数秒以内。</p>
    <p style="margin: 10px 0px">2. 根据需求进行调研,并开发了定制化功能模块。利用Fabric.js绘制产品的复杂效果,同时保证适配PC和移动端。</p>
   <p style="margin: 10px 0px">3. 抽出常用组件,工具类,hook,同时使用七牛云Oss整理资源图,压缩,裁剪图片,自动传包,Webpack常规优化。</p>
    <p style="margin: 10px 0px">4. 将模块集成到对应shopify等站点,与后端对接商品合成图,根据测试单调整前端生成合成图。</p>
    <p style="margin: 10px 0px">5. 使用Gtag记录商品点击率,加车率等统计埋点,做出数据统计为产品功能拓展提供支持。</p>`,
    tags: [
      "React",
      "Canvas",
      "Fabric.js",
      "Dumi",
      "Three.js",
      "Typescript",
      "D3",
      "I18n",
    ],
    imageUrl: legomoduleImg,
    link: "https://my-modules.netlify.app/modules/lego-mini",
    link2: "https://minefigs.com/products/custom-lego-minifigures",
  },
  {
    title: "设计器平台",
    description:
      "平台包含主图中心,定制设计,素材维护,异常订单,店铺管理等板块,提供主图设计平台,为产品经理和运营提供快速搭建定制化产品功能,提供产品快速集成,站点快速同步功能,为工厂提供测试单反馈等,提高各方面工作效率。",
    tags: ["React", "Canvas", "Fabric.js", "Umi", "Three.js", "Redux"],
    imageUrl: designerPlatform,
    content: `<p style="margin: 10px 0px">1. 设计和开发c端的定制功能组件并发布为一个npm包。优化这些组件的性能和可定制性,使其更易于集成和扩展。</p>
    <p style="margin: 10px 0px">2. 根据RABC权限体系划分用户权限,推出通用登录方案,用户退出方案,动态侧边栏方案等,解决平台常见问题,让用 户更方便操作平台。</p>
   <p style="margin: 10px 0px"> 3. 开发B端定制功能可视化拖拽平台,并提供商品变体、模板、加车直达工厂等配置,以月为单位分析新品上架数量,分析 出平台能有效增加平均80%的产品上新率。</p>
    <p style="margin: 10px 0px">4. 使用Echarts开发各个可视化数据平台,对商品定制,异常订单,工厂测试单反馈进行数据分析和报告,解决大量数据 分析难点,对运营提供有效分析并减轻40%工作量。</p>
    <p style="margin: 10px 0px">5. 使用gitlab webhook接入飞书机器人,对特定群提供项目更新与发布相关信息,帮助产品和测试快速跟进产品开发进度。
    </p>`,
    link: "",
    link2: "",
  },
  {
    title: "DiscuzQ",
    description:
      "本项目是腾讯云开源项目,旨在重构社区产品,使用前后端开发模式,降低开发者们开发和使用者的难度,以轻量化,易变 现的目的来打造出新的论坛产品,也为DZQ论坛电商版打下基础。",
    tags: ["Vue", "Nuxt.js", "Vue", "Vuex", "Element UI", "Vue-Router"],
    imageUrl: dzqImg,
    content: `<p style="margin: 10px 0px">1. 负责开发用户名、手机号、微信扫码登录注册、个人中心和个人主页等板块。使用 Vuex 集中管理用户信息、登录状态、个人设置等数据,实现组件之间的共享状态。</p>
    <p style="margin: 10px 0px">2. 通过配置 Axios 请求拦截器,对请求进行统一处理,添加认证信息、错误处理。通过白名单配置进行路由鉴权处理,确 保只有经过授权的用户才能访问特定页面或功能。</p>
    <p style="margin: 10px 0px">3. 使用 Shell 脚本实现快速部署和发布,简化了部署流程,提高开发效率和发布效率。</p>
    <p style="margin: 10px 0px">4. 首页使用了vue-virtual-scroller 进行长列表优化,提升了页面的渲染性能和用户体验。同时Webpack 进行压缩 JavaScript 和 Css、代码chunk分块,按需加载组件等处理,进一步优化了页面加载速度和性能。</p>`,
    link: "https://gitee.com/Discuz/Discuz-Q-Web",
    link2: "",
  },
] as const;

export const skillsData = [
  "HTML",
  "CSS",
  "JavaScript",
  "TypeScript",
  "Vue",
  "Vue3",
  "React",
  "Next.js",
  "Node.js",
  "Git",
  "Tailwind",
  "Zod",
  "MongoDB",
  "Redux",
  "Vuex",
  "Express",
  "Canvas",
  "Fabric.js",
  "Framer Motion",
  "Element UI",
  "Ant Design UI",
  "Shadcn UI",
  "Webpack",
  "Rollup",
  "Vite",
  "Vercel",
  "Netlify",
  "Shopify",
  "Shopline",
  "Wechat Mini Program",
  "CET-4",
] as const;

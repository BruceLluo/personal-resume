"use client";

import { LanguageSwitchBase } from "./LanguageSwitchBase";
import { useTranslation } from "../../../i18n/client";

export const Footer = ({ lng }: { lng: string }) => {
  const { t } = useTranslation(lng, "footer");
  return <LanguageSwitchBase t={t} lng={lng} />;
};

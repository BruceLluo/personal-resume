import { useTranslation } from "../../../i18n";
import { LanguageSwitchBase } from "./LanguageSwitchBase";

export const LanguageSwitch = async ({ lng }: { lng: string }) => {
  const { t } = await useTranslation(lng, "footer");
  return <LanguageSwitchBase t={t} lng={lng} />;
};

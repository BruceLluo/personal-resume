import Link from "next/link";
import { Trans } from "react-i18next/TransWithoutContext";
import { languages } from "../../../i18n/settings";
import { TFunction } from "i18next/typescript/t";

export const LanguageSwitchBase = ({
  t,
  lng,
}: {
  t: TFunction<any, any>;
  lng: string;
}) => {
  console.log("footbase", lng);
  return (
    <footer style={{ marginTop: 50 }}>
      {/* <Trans i18nKey="languageSwitcher" t={t}>
        Switch from <strong>{{lng}}</strong> to:{" "}
      </Trans> */}

      {languages
        .filter((l) => lng !== l)
        .map((l, index) => {
          return (
            <button
              className="fixed bottom-20 right-5 bg-white w-[3rem] h-[3rem] bg-opacity-80 backdrop-blur-[0.5rem] border border-white border-opacity-40 shadow-2xl rounded-full flex items-center justify-center hover:scale-[1.15] active:scale-105 transition-all dark:bg-gray-950"
              key={l}
            >
              <span>
                {index > 0 && " or "}
                <Link href={`/${l}`}>{l === "zh" ? "中文" : l}</Link>
              </span>
            </button>
          );
        })}
    </footer>
  );
};

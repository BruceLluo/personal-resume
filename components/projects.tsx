"use client";

import React from "react";
import SectionHeading from "./section-heading";
import { projectsData, projectsDataZh } from "@/lib/data";
import Project from "./project";
import { useSectionInView } from "@/lib/hooks";
import { useActiveSectionContext } from "@/context/active-section-context";
import { useTranslation } from "@/app/i18n/client";


export default function Projects() {
  const { ref } = useSectionInView("Projects", 0.5);
  const {lng} = useActiveSectionContext()
  const projects = lng === 'zh' ? projectsDataZh : projectsData
  const { t } = useTranslation(lng, "project");
  
  return (
    <section ref={ref} id="projects" className="scroll-mt-28 mb-28">
      <SectionHeading>{t('title')}</SectionHeading>
      <div>
        {projects.map((project, index) => (
          <React.Fragment key={index}>
            {/* @ts-ignore */}
            <Project {...project} lng={lng} />
          </React.Fragment>
        ))}
      </div>
    </section>
  );
}

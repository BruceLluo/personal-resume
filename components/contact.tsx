"use client";

import React from "react";
import SectionHeading from "./section-heading";
import { motion } from "framer-motion";
import { useSectionInView } from "@/lib/hooks";
import { sendEmail } from "@/actions/sendEmail";
import SubmitBtn from "./submit-btn";
import toast from "react-hot-toast";
import { useActiveSectionContext } from "@/context/active-section-context";
import { useTranslation } from "@/app/i18n/client";

export default function Contact() {
  const { ref } = useSectionInView("Contact");
  const { lng } = useActiveSectionContext();
  const { t } = useTranslation(lng, "contact");

  return (
    <motion.section
      id="contact"
      ref={ref}
      className="mb-20 sm:mb-28 w-[min(100%,38rem)] text-center"
      initial={{
        opacity: 0,
      }}
      whileInView={{
        opacity: 1,
      }}
      transition={{
        duration: 1,
      }}
      viewport={{
        once: true,
      }}
    >
      <SectionHeading>{t("title")}</SectionHeading>

      <p className="text-gray-700 -mt-6 dark:text-white/80">
        {t("content1")}{" "}
        <a className="underline" href="mailto:295448382@qq.com">
          {t("content2")}
        </a>{" "}
        {t("content3")}
      </p>

      <form
        name="contact"
        data-netlify={true}
        className="mt-10 flex flex-col dark:text-black"
        action={async (formData) => {
          // formData.preventDefault();
          const formDatas = {
            //@ts-ignore
            // senderEmail: formData.target[0].value,
            //@ts-ignore
            // message: formData.target[1].value,
            senderEmail: formData.get("senderEmail"),
            message: formData.get("message"),
          };
          try {
            const response = await fetch("/api/sendEmail", {
              method: "POST",
              body: JSON.stringify(formDatas),
            });

            if (response.ok) {
              const data = await response.json();
              if (data.status === "success") {
                console.log("Response:", data, data.message);
                toast.success(t("success"));
              } else {
                toast.error(data.message);
                return;
              }
            }
          } catch (error) {
            console.error("Error:", error);
          }

          // server action
          // const { error } = await sendEmail(formDatas);
          // if (error) {
          //   toast.error(error);
          //   return;
          // }

          // toast.success(t("success"));
        }}
        method="POST"
      >
        <input
          className="h-14 px-4 rounded-lg borderBlack dark:bg-white dark:bg-opacity-80 dark:focus:bg-opacity-100 transition-all dark:outline-none"
          name="senderEmail"
          type="email"
          required
          maxLength={500}
          placeholder={t("holder1")}
        />
        <textarea
          className="h-52 my-3 rounded-lg borderBlack p-4 dark:bg-white dark:bg-opacity-80 dark:focus:bg-opacity-100 transition-all dark:outline-none"
          name="message"
          placeholder={t("holder2")}
          required
          maxLength={5000}
        />
        <SubmitBtn />
      </form>
    </motion.section>
  );
}

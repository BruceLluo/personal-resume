"use client";

import { useRef, useState } from "react";
import { projectsData } from "@/lib/data";
import Image from "next/image";
import { motion, useScroll, useTransform } from "framer-motion";
import { IoIosCloseCircleOutline } from "react-icons/io";
import ReactHtmlParser from "react-html-parser";

type ProjectProps = (typeof projectsData)[number] & { lng?: string };

export default function Project({
  title,
  description,
  content,
  tags,
  imageUrl,
  link,
  link2,
  lng,
}: ProjectProps) {
  const [show, setShow] = useState(false);
  const ref = useRef<HTMLDivElement>(null);
  const { scrollYProgress } = useScroll({
    target: ref,
    offset: ["0 1", "1.33 1"],
  });
  const scaleProgess = useTransform(scrollYProgress, [0, 1], [0.8, 1]);
  const opacityProgess = useTransform(scrollYProgress, [0, 1], [0.6, 1]);
  const loaderProp = ({ src }: { src: string }) => {
    return src;
  };
  return (
    <>
      <motion.div
        ref={ref}
        style={{
          scale: scaleProgess,
          opacity: opacityProgess,
        }}
        className="group mb-3 sm:mb-8 last:mb-0"
        onClick={() => setShow(true)}
      >
        <section className="bg-gray-100 max-w-[42rem] border border-black/5 rounded-lg overflow-hidden sm:pr-8 relative sm:h-[25rem] hover:bg-gray-200 transition sm:group-even:pl-8 dark:text-white dark:bg-white/10 dark:hover:bg-white/20 cursor-pointer">
          <div className="pt-4 pb-7 px-5 sm:pl-10 sm:pr-2 sm:pt-10 sm:max-w-[50%] flex flex-col h-full sm:group-even:ml-[18rem]">
            <h3 className="text-2xl font-semibold">{title}</h3>
            <p className="mt-2 leading-relaxed text-gray-700 dark:text-white/70">
              {description}
            </p>
            <ul className="flex flex-wrap mt-4 gap-2 sm:mt-auto">
              {tags.map((tag, index) => (
                <li
                  className="bg-black/[0.7] px-3 py-1 text-[0.7rem] uppercase tracking-wider text-white rounded-full dark:text-white/70"
                  key={index}
                >
                  {tag}
                </li>
              ))}
            </ul>
          </div>

          <Image
            src={imageUrl}
            alt="Project I worked on"
            quality={95}
            loader={loaderProp}
            className="absolute hidden sm:block top-8 -right-40 w-[28.25rem] rounded-t-lg shadow-2xl
        transition 
        group-hover:scale-[1.04]
        group-hover:-translate-x-3
        group-hover:translate-y-3
        group-hover:-rotate-2

        group-even:group-hover:translate-x-3
        group-even:group-hover:translate-y-3
        group-even:group-hover:rotate-2

        group-even:right-[initial] group-even:-left-40"
          />
        </section>
      </motion.div>
      {show && (
        <div className=" fixed top-0 right-0 bottom-0 left-0 w-full h-full bg-[rgba(0,0,0,0.8)] z-[999]">
          <div className=" absolute top-[50%] left-[50%] translate-x-[-50%] translate-y-[-50%] max-sm:w-[80%]">
            <IoIosCloseCircleOutline
              className=" absolute right-2 top-1 z-10 w-6 h-6 cursor-pointer"
              onClick={() => setShow(false)}
            />
            <section className="bg-gray-100 max-w-[45rem] border border-black/5 rounded-lg overflow-auto relative sm:h-[45rem] max-sm:max-h-80 transition sm:group-even:pl-8 dark:text-white dark:bg-white/10">
              <div className="pt-4 pb-7 px-5 sm:pl-10 sm:pr-10 sm:pt-10 flex flex-col h-full sm:group-even:ml-[18rem]">
                {/* <h3 className="text-2xl font-semibold">{title}</h3> */}
                <h3 className="text-2xl font-semibold">{"What I Do"}</h3>
                <p className="mt-2 leading-relaxed text-gray-700 dark:text-white/70">
                  {ReactHtmlParser(content)}
                </p>
                <p className=" underline text-blue-500">
                  <a href={link} target="_blank">
                    {link}
                  </a>
                </p>
                <p className=" underline text-blue-500">
                  <a href={link2} target="_blank">
                    {link2}
                  </a>
                </p>
                <ul className="flex flex-wrap mt-4 gap-2 sm:mt-auto">
                  {tags.map((tag, index) => (
                    <li
                      className="bg-black/[0.7] px-3 py-1 text-[0.7rem] uppercase tracking-wider text-white rounded-full dark:text-white/70"
                      key={index}
                    >
                      {tag}
                    </li>
                  ))}
                </ul>
              </div>
            </section>
          </div>
        </div>
      )}
    </>
  );
}

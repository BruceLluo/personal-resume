"use client";

import React from "react";
import SectionHeading from "./section-heading";
import {
  VerticalTimeline,
  VerticalTimelineElement,
} from "react-vertical-timeline-component";
import "react-vertical-timeline-component/style.min.css";
import { experiencesData, experiencesDataZh } from "@/lib/data";
import { useSectionInView } from "@/lib/hooks";
import { useTheme } from "@/context/theme-context";
import { useInView } from "react-intersection-observer";
import { useActiveSectionContext } from "@/context/active-section-context";
import { useTranslation } from "@/app/i18n/client";

function TimelineElement({ item, index }: { item: any; index: number }) {
  const { theme } = useTheme();
  const { ref, inView } = useInView({});

  return (
    <div ref={ref}>
      {/* @ts-ignore */}
      <VerticalTimelineElement
        visible={inView}
        position={(index + 1) % 2 === 0 ? "right" : "left"}
        contentStyle={{
          background:
            theme === "light" ? "#f3f4f6" : "rgba(255, 255, 255, 0.05)",
          boxShadow: "none",
          border: "1px solid rgba(0, 0, 0, 0.05)",
          textAlign: "left",
          padding: "1.3rem 2rem",
        }}
        contentArrowStyle={{
          borderRight:
            theme === "light"
              ? "0.4rem solid #9ca3af"
              : "0.4rem solid rgba(255, 255, 255, 0.5)",
        }}
        date={item.date}
        icon={item.icon}
        iconStyle={{
          background: theme === "light" ? "white" : "rgba(255, 255, 255, 0.15)",
          fontSize: "1.5rem",
        }}
      >
        <h3 className="font-semibold capitalize">{item.title}</h3>
        <p className="font-normal !mt-0">{item.location}</p>
        <p className="!mt-1 !font-normal text-gray-700 dark:text-white/75">
          {item.description}
        </p>
      </VerticalTimelineElement>
    </div>
  );
}

export default function Experience() {
  const { ref } = useSectionInView("Experience");
  const { lng } = useActiveSectionContext();
  const { t } = useTranslation(lng, "experience");
  const experiences = lng === "zh" ? experiencesDataZh : experiencesData;
  return (
    <section id="experience" ref={ref} className="scroll-mt-28 mb-28 sm:mb-40">
      <SectionHeading>{t("title")}</SectionHeading>
      <VerticalTimeline lineColor="">
        {/* @ts-ignore */}
        {experiences.map((item, index) => {
          return <TimelineElement key={index} item={item} index={index} />;
        })}
      </VerticalTimeline>
    </section>
  );
}
